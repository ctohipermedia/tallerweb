<aside class="Sidebar">
	
	<div class="Sidebar-user">
		<figure class="Sidebar-userImg">
			<img src="img/user.png" alt="">
		</figure>
		<div class="Sidebar-userInfo">
			<h3>Nombre del usuario</h3>
			<h4>Cargo</h4>
		</div>
		<div class="Sidebar-userActions">
			<i class="fa fa-user" aria-hidden="true"></i>
			<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
			<i class="fa fa-sign-in" aria-hidden="true"></i>
		</div>
	</div>
	<nav class="Sidebar-nav">
		<p class="Sidebar-navTitle">OPCIONES</p>
		<ul class="Sidebar-navNav">
			<li class="active">
				<a href="escritorio.php">
					Inicio
				</a>
				<ul>
					<li>
						<a href="">Opción 1</a>
					</li>
					<li>
						<a href="">Opción 2</a>
					</li>
					<li>
						<a href="">Opción 3</a>
					</li>
				</ul>
			</li>
			<li>
				<a href="usuarios.php">
					Usuarios
				</a>
			</li>
			<li>
				<a href="productos.php">
					Productos
				</a>
			</li>
			<li>
				<a href="reportes.php">
					Reportes
				</a>
			</li>
		</ul>
	</nav>

</aside>
