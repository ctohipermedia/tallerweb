
<!-- Llamamos al archivo que contiene el header (para no escribir a cada rato lo mismo) -->
<?php require_once('header.php'); ?>

	<!-- Llamamos al archivo que contiene el formulario de login -->
	<section class="u-contenedor">
		<?php require_once('sidebar.php'); ?>
		<div>
			<?php require_once('login.php'); ?>
		</div>
	</section>

<!-- Llamamos al archivo que contiene el footer -->
<?php require_once('footer.php'); ?>
